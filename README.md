# shib_auth - Shibboleth authentication plugin for Roundcube

This plugin makes it possible to use Shibboleth as *alternative*
authentication service for roundcube. It supports two different modes
of operation:
 * Dovecot masteruser
 * Shibboleth serviceToken

## Requirements

 * Apache mod_shib
 * Dovecot IMAP server with master passdb **OR** Shibboleth server with serviceToken support 
 * SMTP server using dovecot auth

## Installation with Dovecot masteruser

 1. Configure Dovecot to provide login with master passdb
    ```
    # master user feature, for shibbolethed roundcube
    # https://wiki.dovecot.org/Authentication/MasterUsers
    auth_master_user_separator = *

    # master user feature, for shibbolethed roundcube
    passdb {
      driver = passwd-file
      # generate with: htpasswd -n -s MASTERUSER
      args = /etc/dovecot/passdb.masterusers
      # if you want to restrict master login feature to roundcube:
      #override_fields = allow_nets=127.0.0.1/32
      master = yes
      pass = yes
    }
    ```
 1. Install plugin to `/PATH/TO/roundcube/plugins/shib_auth`
 1. Create `/PATH/TO/roundcube/plugins/shib_auth/config.inc.php`
    ```php
    <?php
    // configuration for shib_auth
    // See config.inc.php.dist for instructions
    // Check the access right of the file if you put sensitive information in it.
    $config=array();
    $config['dovecot_master_username'] = 'MASTERUSER';
    $config['dovecot_master_password'] = 'MASTERPASSWORD';
    $config['dovecot_master_user_separator'] = '*';
    $config['user_env'] = 'REMOTE_USER';
    ?>
    ```
 1. Configure Shibboleth to set `REMOTE_USER` environment to the
    user's IMAP login
 1. Enable and configure apache's mod_shib:
    ```apache
    <Directory /PATH/TO/roundcube/public_html/>
        AuthName "Roundcube Webmail"
        AuthType shibboleth
        ShibUseHeaders On
        ShibUseEnvironment On
        ShibRequestSetting requireSession 0
        Require shibboleth
    </Directory>
    ```


## Installation with Shibboleth serviceToken

 1. Configure your IdP service to generate serviceToken for successful
    logins, see [IdP serviceToken setup](IdP-serviceToken.md)
 1. Configure Dovecot to provide login with sql passdb using Shibboleth's serviceToken
    ```
    passdb {
      # your standard passdb settings
      # if auth is not successful, do not fail but continue with next passdb (SQL) to check for Shibboleth auth token
      result_success = return-ok
      result_failure = continue
      result_internalfail = return-fail
    }
    passdb {
      driver = sql
      args = /etc/dovecot/pgsql.conf # or mysql
      # if you want to restrict serviceToken login feature to roundcube:
      #override_fields = allow_nets=127.0.0.1/32
      skip = authenticated
      result_success = return-ok
      result_internalfail = return-fail
      result_failure = return-fail
    }
    ```
 1. Configure Dovecot's SQL plugin
    ```
    # see https://doc.dovecot.org/configuration_manual/authentication/sql/#authentication-sql
    driver = pgsql
    connect = DBSERVER dbname=DBNAME user=DBUSER password=DBPASS
    # the "password" contains the entity_id AND the token, seperated by space ' '.
    # As the token only contains alphanum char, we use split_part to extract entity_id and token from the password string
    password_query = SELECT NULL AS password, 'Y' AS nopassword, '%u' AS userid \
      WHERE authenticate('%u', split_part('%w', ' ', 2), split_part('%w', ' ', 1))
    ```
    Replace `DBSERVER`, `DBNAME`, `DBUSER` and `DBPASS` appropriately.
 1. Install plugin to `/PATH/TO/roundcube/plugins/shib_auth`
 1. Create `/PATH/TO/roundcube/plugins/shib_auth/config.inc.php`
    ```php
    <?php
    // configuration for shib_auth
    // See config.inc.php.dist for instructions
    // Check the access right of the file if you put sensitive information in it.
    $config=array();
    $config['user_env'] = 'REMOTE_USER';
    $config['servicetoken_env'] = 'serviceToken';
    $config['entity_id_env'] = 'callingEntityId';
    ?>
    ```
 1. Configure Shibboleth to set `REMOTE_USER`, `serviceToken` and
    `callingEntityId` environment to the user's IMAP login, generated
    serviceToken and calling entity id.
 1. Enable and configure apache's mod_shib:
    ```apache
    <Directory /PATH/TO/roundcube/public_html/>
        AuthName "Roundcube Webmail"
        AuthType shibboleth
        ShibUseHeaders On
        ShibUseEnvironment On
        ShibRequestSetting requireSession 0
        Require shibboleth
    </Directory>
    ```

<!-- vim: set ts=4 sw=4 expandtab fenc=utf8 ff=unix tw=120: -->
