# IdP serviceToken setup

## Postgresql

 1. setup a Postgresql database with the following table:
    ```sql
    CREATE TABLE IF NOT EXISTS service_tokens (
           uid VARCHAR(256),
           password VARCHAR(256),
           expiration TIMESTAMP,
           sp_entity_id VARCHAR(256),
           PRIMARY KEY(uid, sp_entity_id)
    );
    ```
 1. add the following three stored procedures:
    ```sql
    CREATE OR REPLACE FUNCTION random_string(length INTEGER)
        RETURNS text
        LANGUAGE plpgsql
        AS $$
        DECLARE
            chars text[] := '{0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
            result text := '';
            i integer := 0;
        BEGIN
            IF length < 0 THEN
            RAISE EXCEPTION 'Given length cannot be less than 0';
            END if;
            FOR i IN 1..length LOOP
                result := result || chars[1+random()*(array_length(chars, 1)-1)];
            END loop;
            RETURN result;
        END;$$;
    
    CREATE OR REPLACE FUNCTION get_or_create_service_token(p_uid VARCHAR, p_sp_entity_id VARCHAR, p_validity INTERVAL)
        RETURNS VARCHAR
        LANGUAGE plpgsql
        AS $$
        DECLARE
            v_password VARCHAR;
            v_count_result INTEGER;
        BEGIN
            SELECT random_string(30) INTO v_password;
            SELECT count(*) INTO v_count_result
                FROM  service_tokens
                WHERE uid = p_uid
                AND   sp_entity_id = p_sp_entity_id;
     
            IF v_count_result > 0 THEN
                SELECT password INTO v_password
                    FROM service_tokens
                    WHERE uid = p_uid
                    AND   sp_entity_id = p_sp_entity_id;
     
                UPDATE service_tokens
                    SET expiration = LOCALTIMESTAMP + p_validity
                    WHERE uid = p_uid
                    AND   sp_entity_id = p_sp_entity_id;
            ELSE
                INSERT INTO service_tokens
                    (uid, password, expiration, sp_entity_id)
                    VALUES
                    (p_uid, v_password, LOCALTIMESTAMP + p_validity, p_sp_entity_id);
            END IF;
            RETURN v_password;
        END;$$;
     
    CREATE OR REPLACE FUNCTION authenticate(p_uid VARCHAR, p_password VARCHAR, p_sp_entity_id VARCHAR)
        RETURNS BOOLEAN
        LANGUAGE plpgsql
        AS $$
        DECLARE
            v_auth_count INTEGER := 0;
            v_return BOOLEAN := FALSE;
        BEGIN
            SELECT count(*) INTO v_auth_count
                FROM service_tokens
                WHERE uid = p_uid
                AND   password = p_password
                AND   sp_entity_id = p_sp_entity_id
                AND   expiration > LOCALTIMESTAMP;
     
            IF v_auth_count = 1 THEN
                v_return = TRUE;
            ELSE
                v_return = FALSE;
            END IF;
            RETURN v_return;
        END;$$;
    ```
 1. Add roles for access from IdP and IMAP server as appropriate.


## Shibboleth

 1. add a `DataConnector` in your attribute-resolver config
    to access the SQL stored procedure `get_or_create_service_token`:
    ```xml
    <DataConnector id="service_tokens" xsi:type="RelationalDatabase">
        <FailoverDataConnector ref="failover_service_tokens"/>
        <SimpleManagedConnection
        jdbcDriver="org.postgresql.Driver" jdbcURL="jdbc:postgresql://DBSERVER:5432/DBNAME"
        jdbcUserName="DBUSER" jdbcPassword="DBPASS" />
        <QueryTemplate><![CDATA[
            SELECT get_or_create_service_token('$resolutionContext.principal', '$resolutionContext.attributeRecipientID', '%{idp.session.timeout}');
        ]]></QueryTemplate>
        <Column columnName="get_or_create_service_token" attributeID="serviceToken" />
    </DataConnector>
 
    <DataConnector id="failover_service_tokens" xsi:type="ScriptedDataConnector">
        <Script><![CDATA[
            IdPAttribute = Java.type("net.shibboleth.idp.attribute.IdPAttribute");
            StringAttributeValue = Java.type("net.shibboleth.idp.attribute.StringAttributeValue");
            HashSet = Java.type("java.util.HashSet");
 
            attr = new IdPAttribute("serviceToken");
            set = new HashSet(1);
            set.add(new StringAttributeValue("Error: the service is currently not available!"));
            attr.setValues(set);
            connectorResults.add(attr);
        ]]></Script>
    </DataConnector>
    ```
    Replace `DBSERVER`, `DBNAME`, `DBUSER` and `DBPASS` appropriately.
 1. add `serviceToken` attribute in your attribute-resolver config:
    ```xml
    <AttributeDefinition xsi:type="Simple" id="serviceToken">
        <InputDataConnector ref="service_tokens" attributeNames="serviceToken" />
        <AttributeEncoder xsi:type="SAML2String" name="serviceToken" friendlyName="serviceToken" encodeType="false" />
    </AttributeDefinition>
    ```
 1. if you use an `AttributeFilterPolicy`, the `serviceToken`
    attribute must be exported to the webmail server in it.
 1. If you use `Consent` (see intercept/consent-intercept-config.xml)
    the `serviceToken` attribute must be included there.
