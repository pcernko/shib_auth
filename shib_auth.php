<?php

/**
 * Shibboleth Authentication, based on HTTP Basic Authentcation by Thomas Bruederli
 *
 * Make use of an existing Shibboleth authentication and perform login for the user using dovecot's masterpassword feature
 *
 * Configuration:
 * $config['dovecot_master_username'] = 'masteruser';
 * $config['dovecot_master_password'] = 'MasterUserPassword';
 * $config['dovecot_master_user_separator'] = '*';
 * $config['user_env'] = 'REMOTE_USER';
 *
 * For other configuration options, see config.inc.php.dist!
 *
 * @license GNU GPLv3+
 * @author Patrick Cernko
 */
class shib_auth extends rcube_plugin
{
    private $redirect_query;
    private $user_env;
    private $mastersep;
    private $masteruser;
    private $masterpass;
    private $servicetoken_env;
    private $entity_id_env;

    function log($message) {
        rcmail::write_log('shib_auth', 'shib_auth::' . $message);
    }

    function init()
    {
        $this->log('init()');

        $this->add_hook('startup',             array($this, 'startup'));
        $this->add_hook('authenticate',        array($this, 'authenticate'));
        $this->add_hook('logout_after',        array($this, 'logout_after'));
        $this->add_hook('login_after',         array($this, 'login_after'));
        $this->add_hook('login_failed',        array($this, 'login_failed'));
        $this->add_hook('storage_connect',     array($this, 'storage_connect'));
        $this->add_hook('smtp_connect',        array($this, 'smtp_connect'));
        $this->add_hook('managesieve_connect', array($this, 'managesieve_connect'));
        // only required without 'require valid-user' in apache config
        $this->add_hook('template_container',  array($this, 'template_container'));
        // Load plugin's config file
        $this->load_config();
        $this->user_env   = rcmail::get_instance()->config->get('user_env');
        $this->mastersep  = rcmail::get_instance()->config->get('dovecot_master_user_separator');
        $this->masteruser = rcmail::get_instance()->config->get('dovecot_master_username');
        $this->masterpass = rcmail::get_instance()->config->get('dovecot_master_password');
        $this->servicetoken_env = rcmail::get_instance()->config->get('servicetoken_env');
        $this->entity_id_env    = rcmail::get_instance()->config->get('entity_id_env');

        $this->log('init() returning');
    }

    function get_env($env) {
        if (empty($_SERVER[$env])) {
            $this->log('get_env() "' . $env . '" not in _SERVER!');
            return null;
        }
        // DEBUG with secrets: $this->log('get_env(): found ' . $env . '="' . $_SERVER[$env] . '"');
        $this->log('get_env(): found ' . $env . '="<hidden>"');
        return $_SERVER[$env];
    }

    function get_username() {
        return $this->get_env($this->user_env);
    }

    function gen_user($username) {
        if ($this->masteruser != null) {
            $masterlogin = $username . $this->mastersep . $this->masteruser;
            $this->log('gen_user(): using masterlogin: ' . $masterlogin);
            return $masterlogin;
        }
        $this->log('gen_user(): using login: ' . $username);
        return $username;
    }

    function gen_pass() {
        if ($this->masteruser != null) {
            $this->log('gen_pass(): using masterpass');
            return $this->masterpass;
        }
        $token = $this->get_env($this->servicetoken_env);
        // DEBUG with secrets: $this->log('gen_pass(): found token "' . $token . '"');
        $entity_id = $this->get_env($this->entity_id_env);
        // DEBUG with secrets: $this->log('gen_pass(): found entity_id "' . $entity_id . '"');
        return $entity_id . ' ' . $token;
    }

    function shib_logout() {
        $url = '/Shibboleth.sso/Logout';
        $this->log('shib_logout(): redirecting to ' . $url);
        header("Location: " . $url, true, 307);
        exit;
    }
    
    function startup($args)
    {
        $this->log('startup()');

        if ($this->get_username()) {
            // handle login action
            if (empty($_SESSION['user_id'])) {
                $this->log('startup(): not user_id in _SESSION');
                $args['action']       = 'login';
            }
        } else {
            $this->log('startup(): no username found in _SERVER');
            // only required WITH 'require valid-user' in apache config
            //$args['action']       = 'logout';
        }
        // always store query string to use if logged in via shibboleth later
        $query = $_SERVER['QUERY_STRING'];
        // query format: _task=TASKNAME(&_param=value...)
        // only store query if '_task' is not 'login' or 'logout'
        $store_query = true;
        foreach (explode('&', $query) as $param) {
            $kv = explode('=', $param, 2);
            if ($kv[0] != '_task') {
                continue;
            }
            if (in_array($kv[1], array('login', 'logout'))) {
                $store_query = false;
            }
        }
        if ($store_query) {
            $this->redirect_query = $query;
            // DEBUG with secrets: $this->log('startup(): stored redirect_query=' . $this->redirect_query);
            $this->log('startup(): stored redirect_query=<hidden>');
        } else {
            $this->log('startup(): NOT storing query string "' . $query . '" to avoid login loop and CSRF alerts');
        }

        $this->log('startup() returning');
        return $args;
    }
    
    function authenticate($args)
    {
        $this->log('authenticate()');

        if ($username = $this->get_username()) {
            $args['user'] = $username;
            $args['pass'] = 'using master password';
            // only disabled cookiecheck and set "valid" if Shibboleth login detected
            $args['cookiecheck'] = false;
            $args['valid']       = true;
        }

        $this->log('authenticate() returning');
        return $args;
    }

    function logout_after($args)
    {
        $this->log('logout_after()');

        // redirect to configured URL in order to clear HTTP auth credentials
        if (($username = $this->get_username()) && $args['user'] == $username) {
            $this->log('logout_after(): user == shib_user');
            $this->shib_logout(); // will exit()
        }
        
        $this->log('logout_after() returning');
    }

    function login_after($args)
    {
        $this->log('login_after()');

        if ($this->get_username()) {
            // Redirect to the previous query string, stored in startup()
            // this will bring the user back to where she was before session expired
            $this->log('login_after(): redirect_query=' . $this->redirect_query);
            if ($this->redirect_query) {
                $this->log('login_after() redirecting to ./?' . $this->redirect_query);
                header('Location: ./?' . $this->redirect_query);
                exit;
            }
        }

        $this->log('login_after() returning');
        return $args;
    }

    function login_failed($args)
    {
        $this->log('login_failed()');

        if ($this->get_username()) {
            $this->log('login_failed(): something failed while logging in via shibboleth');
            // we have to logout the user from the Shibboleth session as otherwise, a IMAP/LDAP-Login won't work
            $this->shib_logout(); // will exit()
        }

        $this->log('login_failed() returning');
        return $args;
    }

    function storage_connect($args)
    {
        $this->log('storage_connect()');

        if ($username = $this->get_username()) {
            $args['user'] = $this->gen_user($username);
            $args['pass'] = $this->gen_pass();
        }

        $this->log('storage_connect() returning');
        return $args;
    }

    function managesieve_connect($args)
    {
        $this->log('managesieve_connect()');

        if ($username = $this->get_username()) {
            $args['user']     = $this->gen_user($username);
            $args['password'] = $this->gen_pass();
        }

        $this->log('managesieve_connect() returning');
        return $args;
    }

    function smtp_connect($args)
    {
        $this->log('smtp_connect()');

        if ($username = $this->get_username()) {
            $args['smtp_user'] = $this->gen_user($username);
            $args['smtp_pass'] = $this->gen_pass();
        }

        $this->log('smtp_connect() returning');
        return $args;
    }

    function template_container($attrib)
    {
        $this->log('template_container()');

        $hook = array('content' => '');
        if ($attrib['name'] == 'loginfooter') {
            $this->log('template_container(): adding SSO login button');
            // we must use a anchor tag here to avoid having to mainaction buttons both triggering the form action
            // but we use the same classes for the anchor as for the standard Login button
            $anchor_attr = array(
                'id' => 'rcmloginsso',
                'class' => 'button mainaction w-100 btn-lg btn btn-primary',
                'href' => '/Shibboleth.sso/Login?target=' . urlencode('/?' . $this->redirect_query)
            );
            $hook['content'] = html::p('formbuttons', html::tag('a', $anchor_attr, 'SSO Login'));
        }

        $this->log('template_container() returning');
        return $hook;
    }

}

